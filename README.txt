MITron by Jonathan Hurowitz (jwitz@mit.edu), Kyle Swanson (swansonk@mit.edu), and Alberto Ancona (bancona@mit.edu)

Developed as a final project for MIT 6.177, IAP 2015, between the dates of 1/16/15 and 1/23/15.

To start the game, run "trongame.py" in Python.