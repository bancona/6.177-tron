import pygame, sys, os
from constants import *


class Wall(pygame.sprite.Sprite):

    def __init__(self, color, rect, build_direction=(0,1), thickness=WALL_THICKNESS):
        pygame.sprite.Sprite.__init__(self)
        self.color = color
        self.thickness = thickness
        self.build_direction = build_direction
        self.rect = rect
        self.image = pygame.Surface(rect.size)
        self.image.fill(color)

    def extend(self, dist=1):
        """
        Extends wall by dist amount in the direction self.build_direction.
        Basically makes the wall follow behind its light cycle
        as the light cycle moves.
        """
        added_width = abs(self.build_direction[0])
        added_height = abs(self.build_direction[1])

        if self.build_direction == (1, 0) or self.build_direction == (0, 1):
            topleft = self.rect.topleft
            self.rect.width += added_width
            self.rect.height += added_height
            self.rect.topleft = topleft

        elif self.build_direction == (-1, 0) or self.build_direction == (0, -1):
            bottomright = self.rect.bottomright
            self.rect.width += added_width
            self.rect.height += added_height
            self.rect.bottomright = bottomright

        self.image = pygame.Surface(self.rect.size)
        self.image.fill(self.color)