import pygame, sys
from tron_gui import *
from constants import *

# Main program Loop: (called by new_game())
def main_loop(gui, board, stop, pause):
    gui.clear_screen() # initialize the board to all black
    board.walls.draw(gui.screen) # draw Walls
    board.cycles.draw(gui.screen) # draw LightCycle
    pygame.display.flip() # update gui.screen
    pygame.font.init()
    clock = pygame.time.Clock()

    for x in range (0,3): # shows a countdown until game starts
        font = pygame.font.Font(None, 75)
        countdown = font.render(str(3 - x), True, white, black)
        countdown_rect = countdown.get_rect()
        countdown_rect.centerx = gui.screen.get_width() * ((x + 1)/4.0)
        countdown_rect.centery = (board_height * 0.50)
        gui.screen.blit(countdown, countdown_rect)
        pygame.display.flip()
        pygame.time.wait(1000)

    while True:
        if stop == True: # called whenever a player dies.
            winners = []
            for cycle in board.cycles:
                if cycle.is_alive:
                    winners.append(cycle)
            if len(winners) > 1: # if two or more players are still alive,
                stop = False     # the game does not end

            else: # end of game procedure
                black_cover = pygame.Surface((gui.screen.get_width(), 92))
                black_cover.fill(black)
                black_cover_rect = black_cover.get_rect()
                black_cover_rect.x = 0
                black_cover_rect.y = (board_height)
                gui.screen.blit(black_cover, black_cover_rect)
                pygame.display.flip()

                if len(winners) == 0: # when no one wins the game
                    font = pygame.font.Font(None, 30)
                    if len(board.cycles) == 1:
                        message = "Game Over!"
                    else:
                        message = "It's a tie!"
                    text = font.render(message, True, white, black)
                    textRect = text.get_rect()
                    textRect.x = WIDTH * 1.0/5
                    textRect.centery = (HEIGHT - 1 - 9 * board.border_thickness)
                    gui.screen.blit(text, textRect)

                else: # when one player wins the game
                    winner = winners[0]
                    message = "Player #%d wins!" % (winner.player_number + 1)
                    font = pygame.font.Font(None, 40)
                    text = font.render(message, True, winner.get_color(), black)
                    textRect = text.get_rect()
                    textRect.x = 65
                    textRect.centery = (HEIGHT - 1 - 9 * board.border_thickness)
                    gui.screen.blit(text, textRect)

                end_game_button = GUI.Button("Main Menu", 30, #restart button
                    (gui.screen.get_width() - 35 - 200,
                        HEIGHT - 1 - 9 * board.border_thickness - 25), 200, 50,
                        GUI.CARDINAL, None, True)

                while True:
                    end_game_button.update(gui.screen)
                    pygame.display.flip()
                    e = pygame.event.wait()
                    if e.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                    if end_game_button.is_clicked():
                        gui.clear_screen()
                        return False


        while stop == False:
            gui.clear_screen()

            #The following shows the controls at the bottom of the gui.screen
            keyboard_directionals = pygame.image.load(
                'keyboard_directionals.png').convert_alpha() 
            keyboard_wasd = pygame.image.load(
                'keyboard_wasd.png').convert_alpha()
            keyboard_ijkl = pygame.image.load(
                'keyboard_ijkl.png').convert_alpha()
            keyboard_tfgh = pygame.image.load(
                'keyboard_tfgh.png').convert_alpha()

            control_images = [keyboard_directionals, keyboard_wasd,
                keyboard_ijkl, keyboard_tfgh]
            control_images_rect = []
            for image in control_images:
                control_images_rect.append(image.get_rect())

            font = pygame.font.Font(None, 30)
            image_locations = []
            image_locations.append((220, board_height + 3))
            image_locations.append((220 + gui.screen.get_width() / 2,
                board_height + 3))
            image_locations.append((220,
                board_height + 3 + (HEIGHT - board_height) / 2))
            image_locations.append((220 + gui.screen.get_width() / 2,
                board_height + 3 + (HEIGHT - board_height) / 2))

            text_locations = []
            text_locations.append((20, board_height + 15))
            text_locations.append((20 + gui.screen.get_width() / 2,
                board_height + 15))
            text_locations.append((20,
                board_height + 15 + (HEIGHT - board_height) / 2))
            text_locations.append((20 + gui.screen.get_width() / 2,
                board_height + 15 + (HEIGHT - board_height) / 2))

            for cycle in board.cycles:
                if cycle.is_ai: #shows a different msg for AI than humans
                    msg = "Player " + str(cycle.player_number + 1) + " is AI"
                    text = font.render(msg, True,
                        WALL_COLORS[cycle.player_number], black)
                    textRect = text.get_rect()
                    textRect = text_locations[cycle.player_number]
                    gui.screen.blit(text, textRect)
                else:
                    msg = "Player " + str(cycle.player_number + 1) + " Controls:"
                    text = font.render(msg, True,
                        WALL_COLORS[cycle.player_number], black)
                    textRect = text.get_rect()
                    textRect = text_locations[cycle.player_number]
                    gui.screen.blit(text, textRect)
                    control_images_rect[cycle.player_number] = image_locations[
                        cycle.player_number]
                    gui.screen.blit(control_images[cycle.player_number],
                        control_images_rect[cycle.player_number])

            clock.tick(60)
            keys = []
            for event in pygame.event.get():
                if event.type == pygame.QUIT: #user clicks close
                    stop = True
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN: #this lets user pause and
                    if event.key == pygame.K_p:    #unpause the game by pressing
                        if pause:                  #the key "P"
                            pause = False
                        else:
                            pause = True
                    keys.append(event.key)

            if stop == False and pause == False: #when the loop repeats, and the
                                                 #user hasn't quit or paused
                for key in keys:
                    for LC in board.cycles:
                        if LC.is_ai == False:
                            LC.change_direction(key)
                for LC in board.cycles:
                    if LC.is_ai:
                        LC.change_direction(0)
                for LC in board.cycles:
                    LC.move()
                if board.is_collision() == True: #check for collision, and if
                    stop = True                  #collision occurs, exit game

                board.cycles.draw(gui.screen) 
                board.walls.draw(gui.screen)
                pygame.display.flip() # update gui.screen

    pygame.quit() # closes things, keeps idle from freezing
    sys.exit()
