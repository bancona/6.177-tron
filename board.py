import pygame, sys, os
from wall import *
from light_cycle import *
from constants import *


class Board():
    def __init__(self, size, toggles, border_thickness = 5):
        self.size = size
        self.cycles = pygame.sprite.RenderPlain()
        self.walls = pygame.sprite.RenderPlain()
        self.border_thickness = border_thickness

        for i in xrange(len(toggles)):
            if toggles[i].text == "-":
                continue
            is_ai = toggles[i].text == "AI"
            color = LIGHT_CYCLE_COLORS[i]
            start_dir = 0, 1
            if i == 0:
                x = 0.75*board_width
                y = board_height/2
                start_dir = -1, 0
            elif i == 1:
                x = 0.25*board_width
                y = board_height/2
                start_dir = 1, 0
            elif i == 2:
                x = board_width/2
                y = 0.25*board_height
            elif i == 3:
                x = board_width/2
                y = 0.75*board_height
                start_dir = 0, -1
            self.cycles.add(
                LightCycle(self, color, x, y, i, is_ai, start_dir))

        self.add_walls_on_borders();

        for cycle in self.cycles:
            self.walls.add(cycle.walls)

    def add_walls_on_borders(self):
        """
        Adds a set of walls around the board so that if any light cycles hit the
        side of the board, they hit and lose the game.
        """
        self.walls.add(Wall(white,
                        pygame.Rect(0, 0, board_width, self.border_thickness)))
        self.walls.add(Wall(white,
                        pygame.Rect(0, 0, self.border_thickness, board_height)))
        self.walls.add(Wall(white,
                        pygame.Rect(board_width - self.border_thickness, 0,
                                        self.border_thickness, board_height)))
        self.walls.add(Wall(white,
                        pygame.Rect(0, board_height - self.border_thickness,
                                        board_width, self.border_thickness)))
        
    def is_collision(self):
        """
        First adds all walls made by light cycles to self.walls,
        then checks to see if any light cycles have collided with
        each other or a wall. Returns true if they have, and sets
        the light cycles' is_alive fields to False
        """
        for cycle in self.cycles:
            self.walls.add(cycle.walls)

        is_col = False

        for wall in self.walls:
            for cycle in self.cycles:
                # If wall and cycle collide, cycle is dead
                if wall is not cycle.current_wall \
                        and pygame.sprite.collide_rect(wall, cycle):
                    cycle.is_alive = False
                    is_col = True
                # Checks if two (or more) light cycles collide
                for other_cycle in self.cycles:
                    if cycle is not other_cycle \
                            and pygame.sprite.collide_rect(cycle, other_cycle):
                        cycle.is_alive = False
                        other_cycle.is_alive = False
                        is_col = True

        return is_col

    def would_be_collision(self, LC, num_forward):
        """
        Returns True if in num_forward moves, the light cycle LC will collide
        with a wall.
        """
        for cycle in self.cycles:
            self.walls.add(cycle.walls)

        obstacles = self.walls.copy()
        obstacles.add(self.cycles.copy())

        for obstacle in obstacles:
            if obstacle is LC or obstacle is LC.current_wall:
                continue
            LC_rect = LC.rect.move(LC.direction[0] * num_forward,
                                   LC.direction[1] * num_forward)
            if LC_rect.colliderect(obstacle.rect):
                return True

        return False
