import pygame, sys
from wall import *
from constants import *

class LightCycle(pygame.sprite.Sprite):
    def __init__(self, board, color, row, col, player_number, is_ai,
                 start_direction=(0,1), speed = 1):
        pygame.sprite.Sprite.__init__(self)
        self.color = color
        self.is_ai = is_ai
        self.board = board
        self.location = row, col
        self.image = pygame.Surface((2,2))
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.location
        self.direction = start_direction
        self.walls = pygame.sprite.RenderPlain()
        self.is_alive = True
        self.player_number = player_number
        self.current_wall = self.new_wall()
        self.setup_controls()

    def setup_controls(self):
        """
        Sets self.controls to one of four preset controls, one for each possible
        human player. This allows human players to choose their controls.
        """
        if self.player_number == 0:
            self.controls = {"down": pygame.K_DOWN, "up": pygame.K_UP,
                             "left": pygame.K_LEFT, "right": pygame.K_RIGHT}
        elif self.player_number == 1:
            self.controls = {"down": pygame.K_s, "up": pygame.K_w,
                             "left": pygame.K_a, "right": pygame.K_d}
        elif self.player_number == 2:
            self.controls = {"down": pygame.K_k, "up": pygame.K_i,
                             "left": pygame.K_j, "right": pygame.K_l}
        elif self.player_number == 3:
            self.controls = {"down": pygame.K_g, "up": pygame.K_t,
                             "left": pygame.K_f, "right": pygame.K_h}

    def move(self):
        """
        If the LightCycle is alive, moves LightCycle in self.direction and
        extends self.current_wall behind it. Returns self.location.
        """
        if self.is_alive:
            # Adds self.direction to self.location
            self.location = tuple(map(sum, zip(self.location, self.direction)))
            self.rect.topleft = self.location
            if self.current_wall:
                self.current_wall.extend()
        return self.location

    def rotate_left(self):
        """
        Changes self.direction to the direction 90 degrees counter-clockwise.
        """
        if self.direction == (0, 1):
            self.direction = (1, 0)
        elif self.direction == (1, 0):
            self.direction = (0, -1)
        elif self.direction == (0, -1):
            self.direction = (-1, 0)
        elif self.direction == (-1, 0):
            self.direction = (0, 1)

    def rotate_right(self):
        """
        Changes self.direction to the direction 90 degrees clockwise.
        """
        if self.direction == (0, 1):
            self.direction = (-1, 0)
        elif self.direction == (-1, 0):
            self.direction = (0, -1)
        elif self.direction == (0, -1):
            self.direction = (1, 0)
        elif self.direction == (1, 0):
            self.direction = (0, 1)

    def change_direction(self, key):
        """
        Allows the LightCycle to change direction during its turn.
        If the LightCycle is AI, it tries to move forward, right, then left,
        whichever is available without collision.
        If the LightCycle is human, it checks if a key has been pressed and
        changes direction based on self.controls
        """
        # AI
        if self.is_ai:
            if self.board.would_be_collision(self, 2):
                self.rotate_right()
                if self.board.would_be_collision(self, 2):
                    self.rotate_left()
                    self.rotate_left()
                self.new_wall()

        # Human
        else:
            if key == self.controls["down"] \
                    and self.direction != (0, -1) and self.direction != (0, 1):
                self.direction = (0, 1)
            elif key == self.controls["up"] \
                    and self.direction != (0, 1) and self.direction != (0, -1):
                self.direction = (0, -1)
            elif key == self.controls["left"] \
                    and self.direction != (1, 0) and self.direction != (-1, 0):
                self.direction = (-1, 0)
            elif key == self.controls["right"] \
                    and self.direction != (-1, 0) and self.direction != (1, 0):
                self.direction = (1, 0)
            else:
                return
            self.new_wall()

    def new_wall(self):
        """
        Creates a new wall for the LC instance based on the color of the LC.
        """
        self.current_wall = None
        topleft = self.location
        width = 0
        height = 0

        if self.direction == (0, 1): # going down
            width = WALL_THICKNESS
        elif self.direction == (0, -1): # going up
            width = WALL_THICKNESS
            topleft = topleft[0], topleft[1] + WALL_THICKNESS
        elif self.direction == (1, 0): # going right
            height = WALL_THICKNESS
        elif self.direction == (-1, 0): # going left
            height = WALL_THICKNESS
            topleft = topleft[0] + WALL_THICKNESS, topleft[1]

        self.current_wall = Wall(self.trail_color(),
                                 pygame.Rect(topleft, (width, height)),
                                 self.direction)
        self.walls.add(self.current_wall)
        return self.current_wall

    def get_color(self):
        """
        Returns self.color
        """
        return self.color

    def trail_color(self):
        """
        Returns the color a LC wall will produce
        (it looks like a diluted version of the LC color)
        """
        return tuple(map(sum, zip(self.get_color(), WallTuple)))