import pygame, sys
from constants import *

class GUI:

	CARDINAL = (196, 30, 58)

	def __init__(self, width, height):
		pygame.init()
		self.screen = pygame.display.set_mode((width, height))
		pygame.display.set_caption("6.177: MITron by Kyle Swanson, " +
								   "Alberto Ancona, and Jonathan Hurowitz")

	def main_menu(self, function_names):

		# List of buttons to draw on screen and check for input
		buttons = self.setup_buttons(function_names)

		# List of toggles for AI and players
		player_toggles, player_toggle_labels = self.setup_toggles()

		# Sets up title to be blitted to screen
		title_font = pygame.font.SysFont("arialbold", 120)
		title = title_font.render("6.177: MITron", True, self.CARDINAL)

		screen_width = self.screen.get_width()

		# Loop until one of the buttons is clicked
		while True:

			# Check if any toggles have been selected. If one was toggled,
			# change its type.
			for t in player_toggles:
				if t.is_clicked():
					self.toggle(t)
				t.update(self.screen)

			for l in player_toggle_labels:
				self.screen.blit(l[0], (l[1], l[2]))

			# Check if any buttons have been clicked.
			# If one was clicked, return the function name associated with it
			for b in buttons:
				if b.is_clicked() and self.is_at_least_one_player(player_toggles):
					return b.text, player_toggles
				# Draw the button on the screen
				b.update(self.screen)

			# Draw the title on the screen
			self.screen.blit(title, ((screen_width-title.get_width())/2, 50))

			# Check if someone closed the window.
			# If so, terminate the program
			e = pygame.event.wait()
			if e.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			elif e.type == pygame.KEYDOWN \
					and e.key == pygame.K_RETURN \
					and self.is_at_least_one_player(player_toggles):
				return b.text, player_toggles

			# Display the screen
			pygame.display.flip()

	def toggle(self, t):
		if t.text == "H":
			t.text = "AI"
		elif t.text == "AI":
			t.text = "-"
		elif t.text == "-":
			t.text = "H"
		t.image = t.create_image()

	def is_at_least_one_player(self, player_toggles):
		num_humans = 0
		num_ai = 0
		for t in player_toggles:
			if t.text == "H":
				num_humans += 1
			elif t.text == "AI":
				num_ai += 1
		return num_humans > 0 or num_ai > 0

	def setup_buttons(self, function_names):
		buttons = []

		# Define standard width and height of the buttons
		b_width = 200
		b_height = 80

		screen_width = self.screen.get_width()

		# Create list of buttons down the middle of the screen,
		# each labeled with a function name
		for i in xrange(len(function_names)):
			upper_left = (screen_width-b_width) / 2, 250 + 100*(i+1) - b_height/2
			buttons.append(
				self.Button(function_names[i], 50, upper_left,
							b_width, b_height, self.CARDINAL, None))

		return buttons

	def setup_toggles(self):
		player_toggles = []
		player_toggle_labels = []

		# Define standard width and height of the toggles
		t_width = 70
		t_height = 50

		screen_width = self.screen.get_width()

		for i in xrange(4):
			upper_left = i*(t_width+30) + (screen_width-4*(t_width+20))/2, 200
			toggle_type = "H" if i < 2 else "-"
			player_toggles.append(
				self.Button(toggle_type, 40, upper_left,
							t_width, t_height, LIGHT_CYCLE_COLORS[i], None, True))
			toggle_font = pygame.font.SysFont("arial", 20)
			toggle_image = toggle_font.render("Player #%d" % (i+1), False, self.CARDINAL)
			player_toggle_labels.append((toggle_image, upper_left[0], upper_left[1] - 30))

		return player_toggles, player_toggle_labels

	class Button:

		def __init__(self, text, text_size, upper_left, w, h, background_color,
					 image, does_highlight=True):
			self.text = text
			self.text_size = text_size

			self.upper_left = upper_left
			self.width = w
			self.height = h

			# self.background_color defaulted to (0, 0, 0, 0) if passed None
			self.background_color = background_color or (0, 0, 0, 0)
			# self.image defaulted to result of self.create_image() if passed None
			self.image = image or self.create_image()

			# Creates semi-transparent white cover for button to be used
			# when mouse hovering over button.
			# Only applied if self.does_highlight == True
			self.does_highlight = does_highlight
			self.highlight = pygame.Surface((w, h)).convert_alpha()
			self.highlight.fill((255, 255, 255, 50))

		def is_clicked(self):
			"""
			Returns True if mouse is over the button and the left mouse button
			is pressed. Otherwise returns False.
			"""
			return self.is_hover() and pygame.mouse.get_pressed()[0]

		def is_hover(self):
			"""
			Returns True if mouse is within bounds of the button based on
			self.upper_left and self.width, self.height. Otherwise returns False.
			"""
			mouse_x, mouse_y = pygame.mouse.get_pos()
			return mouse_x >= self.upper_left[0] and \
				   mouse_x <= self.upper_left[0] + self.width and \
				   mouse_y >= self.upper_left[1] and \
				   mouse_y <= self.upper_left[1] + self.height

		def update(self, screen):
			"""
			Draws button on screen, then draws highlight on screen if
			self.does_highlight == True. Highlight is a semi-transparent white
			rectangle over the button.
			"""
			screen.blit(self.image, self.upper_left)
			if self.does_highlight and self.is_hover():
				screen.blit(self.highlight, self.upper_left)

		def create_image(self):
			"""
			Returns a pygame.Surface instance with dimensions
			(self.width, self.height) and with self.text drawn in the center,
			colored with background_color
			"""
			# Creates pygame.Surface filled with self.background_color
			image = pygame.Surface((self.width, self.height)).convert_alpha()
			image.fill(self.background_color)

			# Sets up font and creates a pygame.Surface instance with the text
			# drawn on it.
			font = pygame.font.SysFont("arialbold", self.text_size)
			button_text = font.render(self.text.upper(), True, black, \
									  self.background_color)

			# Draws text on colored image pygame.Surface
			text_size = font.size(self.text.upper())
			image.blit(button_text, ((self.width-text_size[0]) / 2, \
									 (self.height-text_size[1]-font.get_descent()) / 2))
			return image

	def clear_screen(self):
		self.screen.fill((0,0,0))