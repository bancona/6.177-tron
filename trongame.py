import tron_gui
from main_loop import *
from board import *
from constants import *

def new_game(gui, toggles):
	"""
	Starts a new game of Tron with the given players specified by toggles.
	Keeps making a new game as long as main_loop() returns True.
	"""
	play_again = True
	while play_again:
	    board = Board(gui.screen.get_width(), toggles)
	    play_again = main_loop(gui, board, False, False)

def main():
	gui = tron_gui.GUI(WIDTH, HEIGHT)
	callback_function_names = ["START", "EXIT"]
	while True:
		func, toggles = gui.main_menu(callback_function_names)
		if func == "START":
			new_game(gui, toggles)
		elif func == "EXIT":
			pygame.quit()
			sys.exit()

main()